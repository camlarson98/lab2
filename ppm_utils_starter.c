#include "ppm_utils.h"

image_t* read_ppm(FILE* image_file) {
  header_t header = read_header(image_file);
  image_t* image = NULL;
  if (strcmp("P3", header.MAGIC_NUMBER) == 0) {
    image = read_p3(image_file, header);
  } else if (strcmp("P6", header.MAGIC_NUMBER) == 0) {
    image = read_p6(image_file, header);
  }
  return image;
}

header_t read_header(FILE* image_file){
  header_t header;
  fscanf(image_file, "%s %d %d %d", header.MAGIC_NUMBER, &header.HEIGHT, &header.WIDTH, &header.MAX_COLOR);
  return header;
  
}

image_t* read_p3(FILE* image_file, header_t header){
  int size = header.HEIGHT * header.WIDTH;
  image_t* image = (image_t*)malloc(sizeof(image_t*));
  image->header = header;
  image->pixels = (pixel_t*)malloc(sizeof(pixel_t*) * size * 2);
  //Check to see if malloc was successful
  if(image == NULL || image->pixels == NULL){
    printf("Unable to allocate memory. Exiting...\n");
    exit(1);
  }
  
  int r, g, b;

  int i = 0;
  while(i < size){
    fscanf(image_file, "%d %d %d", &r, &g, &b);
    image->pixels[i].R = r;
    image->pixels[i].G = g;
    image->pixels[i].B = b;
    i++;
  }
  

  return image;
}

image_t* read_p6(FILE* image_file, header_t header){
  int size = header.HEIGHT * header.WIDTH;
  //malloc space to read the image and pixels into
  image_t* image = (image_t*)malloc(sizeof(image_t*));
  image->header = header;
  image->pixels = (pixel_t*)malloc(sizeof(pixel_t*) * size * 2);
  //Check to see if malloc was successful
  if(image == NULL || image->pixels == NULL){
    printf("Unable to allocate memory. Exiting...\n");
    exit(1);
  }
  //Local variables
  int i = 0;
  char r = 0, g = 0, b = 0;
  //Reading the image into the allocated memory space
  while(i < size){
    fscanf(image_file, "%c%c%c", &r, &g, &b);
    image->pixels[i].R = (int) r;
    image->pixels[i].G = (int) g;
    image->pixels[i].B = (int) b;
    i++;
  }

  return image;
}

void write_header(FILE* out_file, header_t header){
  fprintf(out_file, "%s\n%d %d\n%d\n", header.MAGIC_NUMBER, header.HEIGHT, header.WIDTH, header.MAX_COLOR);
}

void write_p6(FILE* out_file, image_t* image){
  header_t header = image->header;
  header.MAGIC_NUMBER[1] = '6';
  write_header(out_file, header);
  int i = 0;
  int size = header.HEIGHT * header.WIDTH;
  char r, g, b;
  while(i < size){
    r = (char) image->pixels[i].R;
    g = (char) image->pixels[i].G;
    b = (char) image->pixels[i].B;
    fprintf(out_file, "%c%c%c", r, g, b);
    i++;
  }

}

void write_p3(FILE* out_file, image_t* image){
  header_t header = image->header;
  header.MAGIC_NUMBER[1] = '3';
  write_header(out_file, header);
  int i = 0;
  int size = header.HEIGHT * header.WIDTH;
  int r, g, b;
  while(i < size){
    r = image->pixels[i].R;
    g = image->pixels[i].G;
    b = image->pixels[i].B;
    fprintf(out_file, "%d %d %d\n", r, g, b);
    i++;
  }

}





























