/* Cameron Larson
   
   
   
   */
   
#include "ppm_utils.h"

int main(int argc, char *argv[]){

//Test for the correct number of command line inputs
if( argc != 3 ){
  printf("Error: 2 file names required as command line arguments. Extiting...\n");
  exit(1);
}

  else{
    //Open filenames that were inputted
    FILE* input = fopen(argv[1], "r");
    FILE* output = fopen(argv[2], "w");
    //Test to see if files could be opened
    if(input == 0 || output == 0){
      //Opening failed
      printf("Could not open files. Exiting...\n");
      return 1;
    }

    image_t* image = read_ppm(input);
    header_t header = image->header;
    
    if(strcmp("P3", header.MAGIC_NUMBER) == 0){
      write_p6(output, image);
    }
      else if(strcmp("P6", header.MAGIC_NUMBER) == 0){
        write_p3(output, image);
      }
    
    free(image);
    free(image->pixels);
    fclose(input);
    fclose(output);
  }

return 0;

}